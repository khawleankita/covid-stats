const express = require('express');
const app = express(),
bodyParser = require("body-parser");
const util = require('util'); 
port = 3000;
const request = require('request');
const { json } = require('express');


app.use(bodyParser.json());

app.get('/count', async (req, res) => { 
  try {
    const options = {
        url: 'https://api.apify.com/v2/key-value-stores/toDWvRj1JpTXiM8FF/records/LATEST?disableRedirect=true',
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8'
        }
    }
    await request(options ,  (err, response, body)=> {
        if(body){
            let data = JSON.parse(body)
            res.send(data);
        }
        if(err){
            res.json(null);
        } 

    });
    
  } catch (error) {
    res.json('Error on get');
  }
});

app.get('/latest-data', async (req, res) => {
  try {
    const options = {
        url: 'https://api.apify.com/v2/key-value-stores/tVaYRsPHLjNdNBu7S/records/LATEST?disableRedirect=true',
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8'
        }
    }
    await request(options ,  (err, response, body)=> {
        if(body){
            let data = JSON.parse(body)
            res.send(data);
        }
        if(err){
            res.json(null);
        } 

    });
    
  } catch (error) {
    res.json('Error on get');
  }
});

app.get('/cases-stats', async (req, res) => {
  try {
    const options = {
        url: 'https://api.covid19api.com/dayone/country/india',
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8'
        }
    }
    await request(options ,  (err, response, body)=> {
        if(body){
          let data = JSON.parse(body);
          res.send(data);
        }
        if(err){
            res.json(null);
        } 

    });
    
  } catch (error) {
    res.json('Error on get');
  }
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});