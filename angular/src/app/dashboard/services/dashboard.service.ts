import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getAllCases() {
    return this.http.get(`${environment.api}/cases-stats`)
  }
  count(){
    return this.http.get(`${environment.api}/count`)
  }
  
}
