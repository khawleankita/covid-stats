import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartComponent } from './chart/chart.component';
import { TableComponent } from './table/table.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { AgGridModule } from 'ag-grid-angular';
import { WorldChartComponent } from './world-chart/world-chart.component';
import { ActiveChartComponent } from './active-chart/active-chart.component';
import { RecoveredChartComponent } from './recovered-chart/recovered-chart.component';

@NgModule({
  declarations: [MainLayoutComponent, TableComponent, ChartComponent, WorldChartComponent, ActiveChartComponent, RecoveredChartComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HighchartsChartModule,
    AgGridModule.withComponents([])

  ]
})
export class DashboardModule { }
