import { Component, Input, OnInit } from '@angular/core';
import { GridOptions, GridReadyEvent } from 'ag-grid-community';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() rowData;
  gridApi: any;
  gridColumnApi: any;
  ToggledService: any;
  constructor() { }
  columnDefs = [
    {headerName: 'Region', field: 'region',sortable: true },
    {headerName: 'New Infected', field: 'newInfected',sortable: true },
    {headerName: 'New Recovered', field: 'newRecovered',sortable: true},
    {headerName: 'New Deceased', field: 'newDeceased',sortable: true},
    {headerName: 'Total Infected', field: 'totalInfected',sortable: true },
    {headerName: 'Total Recovered', field: 'recovered',sortable: true},
    {headerName: 'Total Deceased', field: 'deceased',sortable: true},
  ];

  gridOptions: GridOptions = {
    suppressDragLeaveHidesColumns: true,
    animateRows: true,
    headerHeight: 45,
    context: {
      componentParent: this
    },  
    defaultColDef: {
      sortingOrder: ['asc', 'desc'],
      sortable: true,
      resizable: true
    }
  };
  ngOnInit(): void {
    console.log(this.rowData);
    
  }
  gridReady(params: GridReadyEvent) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }

  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
}
