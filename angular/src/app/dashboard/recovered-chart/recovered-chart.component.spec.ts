import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveredChartComponent } from './recovered-chart.component';

describe('RecoveredChartComponent', () => {
  let component: RecoveredChartComponent;
  let fixture: ComponentFixture<RecoveredChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveredChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveredChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
