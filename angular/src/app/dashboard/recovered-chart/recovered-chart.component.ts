import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-recovered-chart',
  templateUrl: './recovered-chart.component.html',
  styleUrls: ['./recovered-chart.component.scss']
})
export class RecoveredChartComponent implements OnInit {
  @Input() chartData1 ;
  title = "app";
  chart;
  updateFromInput = false;
  Highcharts = Highcharts;
  chartConstructor = "chart";
  chartCallback;
  chartOptions = {
    title:{
      text: null
    },
    series: [],
    plotOptions: {
      series: {
          events: {
              legendItemClick: function() {
                return false;
              }
          }
      }
    },
    xAxis: {
      type: 'datetime',
      title:{
        text: "All recovered cases over time."
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0    
    },
    yAxis: {
      title:{
        text: "Cases Count"
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0
    },
  };


  constructor() {
    const self = this;
    this.chartCallback = chart => {
      self.chart = chart;
    };

  }

  ngOnInit() {}
  ngOnChanges() {
    if(this.chartData1 && this.chartData1.length) {
      let arrRec = [];
      for(let item of this.chartData1) {
        let d = Date.parse(item.Date)
        arrRec.push([d,item.Recovered ]);
      }
      this.onInitChartRecovered(arrRec);
    }
  }
  onInitChartRecovered(modifiedData) {
    const self = this,
    chart = this.chart;

    chart.showLoading();
    setTimeout(() => {
      chart.hideLoading();
      self.chartOptions.series = [
            {
              name: 'Recovered Cases',
              data: modifiedData,
              type: 'line'
            },
      ];
      self.updateFromInput = true;
    }, 2000);
  }


}