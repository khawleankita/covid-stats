import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { DashboardService } from '../services/dashboard.service';
@Component({ 
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit , OnDestroy{
  regionData;
  subscriptions: Subscription[] = [];
  countData;
  worldChartData;
  allCasesChartData;

  arrRec:any;
  arrAct:any;
  arrDea:any;
  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.casesGraph();
    this.count();
    setInterval(() => this.fetchData() , 900000);// run every 15 min
  }
  fetchData() : void {
    this.casesGraph();
    this.count();
  }
  scroll(id){
    document.getElementById(`${id}`).scrollIntoView({
      behavior: 'smooth'
    });
  }
  casesGraph() {
    this.subscriptions.push(
      this.dashboardService.getAllCases().subscribe((res: any)=>{
        if(res) {
          this.allCasesChartData = res;
        }
      }, err=> {
        console.log('error while data fetch');
        
      }
      ))
  }
  count() {
    this.subscriptions.push(
      this.dashboardService.count().subscribe((res: any)=>{
        if(res) {
          this.countData = res;
          this.regionData = res.regionData;
        }
      }, err=> {
        console.log('error while data fetch');
        
      }
      ))
  }
  // worldChart() {
  //   this.subscriptions.push(
  //     this.dashboardService.getCountryData().subscribe((res: any)=>{
  //       if(res) {
  //         this.worldChartData = res.Countries;
  //       }
  //     }, err=> {
  //       console.log('error while data fetch');
        
  //     }
  //     ))
  // }
  ngOnDestroy() {
    if (this.subscriptions.length > 0) {
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
  }
}
