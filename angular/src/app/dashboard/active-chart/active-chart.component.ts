import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-active-chart',
  templateUrl: './active-chart.component.html',
  styleUrls: ['./active-chart.component.scss']
})
export class ActiveChartComponent implements OnInit {
  @Input() chartData3 ;
  title = "app";
  chart;
  updateFromInput = false;
  Highcharts3 = Highcharts;
  chartConstructor = "chart";
  chartCallback;
  chartOptions = {
    title:{
      text: null
    },
    series: [],
    plotOptions: {
      series: {
          events: {
              legendItemClick: function() {
                return false;
              }
          }
      }
    },
    xAxis: {
      type: 'datetime',
      title:{
        text: "All active cases over time."
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0    
    },
    yAxis: {
      title:{
        text: "Cases Count"
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0
    },
  };


  constructor() {
    const self = this;
    this.chartCallback = chart => {
      self.chart = chart;
    };

  }

  ngOnInit() {}
  ngOnChanges() {
    if(this.chartData3 && this.chartData3.length) {
      let arrAct = [];
      for(let item of this.chartData3) {
        let d = Date.parse(item.Date)
        arrAct.push([d,item.Active ]);
      }
      this.onInitChartActive(arrAct);
    }
  }
  onInitChartActive(modifiedData) {
    const self = this,
    chart = this.chart;

    chart.showLoading();
    setTimeout(() => {
      chart.hideLoading();
      self.chartOptions.series = [
            {
              name: 'Active Cases',
              data: modifiedData,
              type: 'line',
            },
      ];
      self.updateFromInput = true;
    }, 2000);
  }


}