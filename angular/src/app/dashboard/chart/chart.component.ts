import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() chartData ;
  title = "Deceased Chart";
  chart;
  updateFromInput = false;
  Highcharts = Highcharts;
  chartConstructor = "chart";
  chartCallback;
  chartOptions = {
    title:{
      text: null
    },
    series: [],
    plotOptions: {
      series: {
          events: {
              legendItemClick: function() {
                return false;
              }
          }
      }
    },
    xAxis: {
      type: 'datetime',
      title:{
        text: "All deceased cases over time."
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0    
    },
    yAxis: {
      title:{
        text: "Cases Count"
      },
      tickLength: 0,
      gridLineWidth: 0,
      minorGridLineWidth : 0
    },
  };


  constructor() {
    const self = this;
    this.chartCallback = chart => {
      self.chart = chart;
    };

  }

  ngOnInit() {}
  ngOnChanges() {
    if(this.chartData && this.chartData.length) {
      let arrDea = [];
      for(let item of this.chartData) {
        let d = Date.parse(item.Date)
        arrDea.push([d,item.Deaths ]);
      }
      this.onInitChartDeacesed(arrDea);
    }
  }
  onInitChartDeacesed(modifiedData) {
    const self = this,
    chart = this.chart;

    chart.showLoading();
    setTimeout(() => {
      chart.hideLoading();
      self.chartOptions.series = [
            {
              name: 'Deceased Cases',
              data: modifiedData,
              type: 'line'
            },
      ];
      self.updateFromInput = true;
    }, 2000);
  }


}